package com.cts.jdevaluator.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

public class Deliverablestest {
	String path;
	InputStream inputStream;
	File f;
	public String getPath() {
		try {
			Properties prop = new Properties();
			String propFileName = "util.properties";
 			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 			path = prop.getProperty("filePath");
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return path;
	}
	public boolean isFileExist() {
		
		boolean ans=false;
	    try
        {
	    	
	      File  file=new File(getPath());
	        f=file;
	        
	        ans= file.exists();
         } catch (Exception e){
        	 e.printStackTrace();
	      }
	    return ans;
	}
	public boolean hasSvnUrl() {
		try {
			Scanner input=new Scanner(f);
			
			while(input.hasNext()) {
				if(input.nextLine().indexOf("<remote>file:///C:\\DevOps_Subversion_Repository</remote>")>=0) { 
	
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	public void createReport(){

	}
}
